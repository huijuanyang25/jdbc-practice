package db_connect;

import model.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.List;

class StudentRepoTest {
    private static  StudentRepo studentRepo;
    
    @BeforeAll
    static void init() {
        studentRepo = new StudentRepo();
    }
    
    @Test
    void should_return_true_when_add_student() {
        Student student = new Student();
        student.setName("李四");
        student.setAge(18);
        student.setGender("女");
        student.setPhone("15567879876");
        boolean output = studentRepo.addStudent(student);
        Assertions.assertTrue(output);
    }
    
    @Test
    void should_return_students_when_find_students() {
        List<Student> students = studentRepo.findStudents();
        Student student = students.stream().filter(s -> s.getName().equals("李四")).findFirst().get();
        
        Assertions.assertNotNull(student);
        Assertions.assertEquals(18, student.getAge());
        Assertions.assertEquals("女", student.getGender());
        Assertions.assertEquals("15567879876", student.getPhone());
    }

    @Test
    void should_return_true_when_update_student() {
        List<Student> students = studentRepo.findStudents();
        Student student = students.stream().filter(s -> s.getName().equals("李四")).findFirst().get();
        student.setName("王五");
        student.setAge(66);
        student.setGender("女");
        student.setPhone("15567879876");
        boolean output = studentRepo.updateStudent(student);
        Assertions.assertTrue(output);
    }
    
    @Test
    void should_return_true_when_delete_student() {
        List<Student> students = studentRepo.findStudents();
        Student student = students.stream().filter(s -> s.getName().equals("王五")).findFirst().get();
        boolean output = studentRepo.deleteStudent(student);
        Assertions.assertTrue(output);
    }
}