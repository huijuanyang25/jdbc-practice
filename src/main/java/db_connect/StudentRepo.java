package db_connect;

import model.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StudentRepo {
    private DbConnector dbConnector;
    
    public StudentRepo() {
        dbConnector = new DbConnector();
    }
    
    
    public boolean addStudent(Student student) {
        // Need to be implemented
        String querySQL = "INSERT INTO student(name, age, sex, phone) VALUES(?, ?, ?, ?)";
        int addResult = 0;

        try {
            Connection con = dbConnector.createConnect();
            PreparedStatement preparedStatement = con.prepareStatement(querySQL);
            preparedStatement.setString(1, student.getName());
            preparedStatement.setInt(2, student.getAge());
            preparedStatement.setString(3, student.getGender());
            preparedStatement.setString(4, student.getPhone());
            addResult = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.print("Failed!");
        }
        return addResult > 0 ? true : false;

    }
    
    public List<Student> findStudents() {
        // Need to be implemented
        List<Student> studentList = new ArrayList<>();
        String querySQL = "SELECT * FROM student";
        try {
            Connection con = dbConnector.createConnect();
            Statement statement = con.createStatement();
            ResultSet resultSet = statement.executeQuery(querySQL);
            while (resultSet.next()) {
                Student foundStudent = new Student(resultSet.getInt("id"), resultSet.getString("name"),
                        resultSet.getInt("age"), resultSet.getString("sex"),
                        resultSet.getString("phone"));
                studentList.add(foundStudent);
            }
        } catch (SQLException e) {
            System.out.print("Find student failed!");
        }

        return studentList;
    }
    
    public boolean updateStudent(Student student) {
        // Need to be implemented
        String querySQL = "INSERT INTO student(name, age, sex, phone) VALUES (?, ?, ?, ?)";
        int updateResult = 0;
        try {
            Connection con = dbConnector.createConnect();
            PreparedStatement preparedStatement = con.prepareStatement(querySQL);
            preparedStatement.setString(1, student.getName());
            preparedStatement.setInt(2, student.getAge());
            preparedStatement.setString(3, student.getGender());
            preparedStatement.setString(4, student.getPhone());
            updateResult = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.print("Update student failed!");
        }

        return updateResult > 0 ? true : false;
    }
    
    public boolean deleteStudent(Student student) {
        // Need to be implemented
        String querySQL = "DELETE FROM student WHERE name = ?";
        int updateResult = 0;
        try {
            Connection con = dbConnector.createConnect();
            PreparedStatement preparedStatement = con.prepareStatement(querySQL);
            preparedStatement.setString(1, student.getName());
            updateResult = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.print("Delete student failed!");
        }
        return updateResult > 0 ? true : false;
    }
}
