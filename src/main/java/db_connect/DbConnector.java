package db_connect;

import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnector {
    
    private DbConfig dbConfig;
    
    public DbConnector() {
        Yaml yaml = new Yaml();
        InputStream inputStream = this.getClass()
                .getClassLoader()
                .getResourceAsStream("application.yml");
        this.dbConfig = yaml.load(inputStream);
    }
    
    public DbConfig getDbConfig() {
        return dbConfig;
    }
    
    public Connection createConnect() throws SQLException {
        // Need to be implement
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(this.dbConfig.getDatabaseURL(),
                    this.dbConfig.getUsername(),
                    this.dbConfig.getPassword());
        } catch (SQLException e) {
            System.out.println("Connection failed!");
        }

        return conn;
    }
}
